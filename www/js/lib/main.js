 /* START main.js */
 var lock = true;

//CORE
$(document).ready(function() {
    
    
    //MAIN
    $.nette.init();
    
    initOwlCarousel();
    
    initSelectBox("selectLangBox","selectLangButton");
    initSelectBox("selectCategoryBox","selectCategoryButton");
    initSelectBox("selectCategoryFilterBox","selectCategoryFilterButton");
    
    
    //CHECKBOXES
    $( document ).on( "click", ".box .button" , function() {
        
        if($(this).find( "input[type='checkbox']" ).attr( "data-check" )==="false" )
        {
            $(this).find( "input[type='checkbox']" ).prop( "checked" , true);
            $(this).find( "input[type='checkbox']" ).attr( "data-check" , "true" );
        } 
        else
        {
            $(this).find( "input[type='checkbox']" ).prop( "checked" , false);
            $(this).find( "input[type='checkbox']" ).attr( "data-check" , "false" );
        }
    });
    $( ".box .button" ).each(function(){
        $pointer = $(this).find("input[type='checkbox']");
        if($pointer.prop("checked"))
        {
            $pointer.attr( "data-check" , "true" );
        }
    });
    
    
    //POPUPS
    $( document ).on( "click", ".closeCross", function() {
        $(this).parents().eq(2).remove();
    });
    $( document ).on( "click", ".blackWrap", function() {
        $(this).parent().remove();
    });
    
    
    //SELECT BOXES
    $( document ).on( "click", ".selectBox .submenu li" , function() {
        var $parent = $(this).parent();
        $parent.find( "> li" ).each(function() {
            $(this).removeClass("inactive");
        });
        $(this).addClass("inactive");
        $parent.parent().find(".button span").text($(this).text());
        $parent.parent().find(".button input").val(parseInt($(this).attr( "data-id" )));
        $parent.hide();
        $parent.parent().find(".button").attr("data-lock","0");
    });
    
    
    //SELECT LANG 
    $( document ).on( "click", ".selectBox .selectLangBox li" , function() {
        var $head = $(this).parents().eq(2);
        $head.find( "> .button" ).each(function() {
            $(this).removeClass("inactive");
        });
        $head.find( "> .button span:contains("+$(this).text()+")" ).parent().find( "input" ).prop( "checked" , false).attr( "data-check" , "false" ).parent().addClass("inactive");
    });
    
    
    //TABS
    $( document ).on( "click", ".tabs li" , function() {
        
        if($(this).hasClass("file")) {
            $(this).parent().parent().find( ".inputArea input" ).removeClass("inactive");
            $(this).parent().parent().find( ".inputArea textarea" ).addClass("inactive");
        } 
        else {
            $(this).parent().parent().find( ".inputArea input" ).addClass("inactive");
            $(this).parent().parent().find( ".inputArea textarea" ).removeClass("inactive");
        }
        $(this).parent().find( "li" ).each(function() {
            $( this ).removeClass("active");
        });
        $(this).addClass("active");
    });
    
    
    //NOTIFY
    $( document ).on("click", ".notify", function(e) {

        if (confirm("Opravdu chcete upozornit překladatele?") == true) {
            $(this).parent().find(".signalNotify").click();
            alert("Překladatel byl uporněn.");
        } else {
            return false;
        }
        e.preventDefault();
        
    });
    
    
    //EDIT DEMAND
    $( document ).on("click", ".editDemand", function() {
        $(this).parents().eq("6").addClass("inactive").parent().find(".editForm").removeClass("inactive");
    });
    
    
    //GLOBAL VAR FOR TRANSLATION VALUE
    var val;
    
    
    //EDIT TRANSLATION
    $( document ).on("click", ".editTranslation", function() {
        $(this).parent().find( "> .button" ).each(function() {
            $(this).removeClass("inactive");
        });
        $(this).addClass("inactive");
        var $textarea = $(this).parents().eq("2").find(".inputArea textarea");
        $textarea.attr("readonly", false);
        val = $textarea.val();
        $textarea.focus().val("").val(val);
    });
    
    
    //CANCEL EDITING TRANSLATION
    $( document ).on("click", ".cancelTranslation", function() {
        $(this).parent().find( "> .button" ).each(function() {
            $(this).addClass("inactive");
        });
        $(this).parent().find( "> .editTranslation" ).removeClass("inactive");
        $(this).parents().eq("2").find(".inputArea textarea").val(val).attr("readonly", true).blur();
    });
    
    
    //EDIT TRANSLATION - FILE
    $( document ).on("click", ".editTranslationFile", function() {
        $(this).parent().find( "> .button" ).each(function() {
            $(this).removeClass("inactive");
        });
        $(this).addClass("inactive");
        $root = $(this).parents().eq("2");
        $root.find(".download").last().find(".button").addClass("inactive").parents().eq("1").find("input[type='file']").removeClass("inactive");
    });
    
    
    //CANCEL EDITING TRANSLATION - FILE
    $( document ).on("click", ".cancelTranslationFile", function() {
        $(this).parent().find( "> .button" ).each(function() {
            $(this).addClass("inactive");
        });
        $(this).parent().find( "> .editTranslationFile" ).removeClass("inactive");
        $root = $(this).parents().eq("2");
        $root.find(".download").last().find(".button").removeClass("inactive").parents().eq("1").find("input[type='file']").addClass("inactive");
    });
    
    
    //CANCEL
    $( document ).on("click", ".cancel", function() {
        $(this).parents().eq("3").addClass("inactive").parent().find(".owlCarousel").removeClass("inactive");;
    });
    
    
    //DELETE
    $( document ).on("click", ".delete", function(e) {

        if (confirm("Opravdu chcete odstranit poptávku?") == true) {
            $(this).parent().find(".signalDelete").click();
        } else {
            return false;
        }
        e.preventDefault();
        
    });
    
});

var initSelectBox = function(bo, butt)
{
    
    $(document).mouseup(function (e) {
        
        var $box = $( "." + bo );

        if (!$box.is(e.target) && !(e.target.getAttribute("data-lock") == 1) && $box.has(e.target).length === 0) {
            $box.hide();
            $( "." + butt ).attr("data-lock","0");
        }
    });
    
    $( document ).on("click", "." + butt, function(e) {
        
        var $box = $(this).parent().find("." + bo);
        
        if($(this).attr("data-lock") == 1) {
            $box.hide();
            $(this).attr("data-lock","0");
        } 
        else {
            $box.show();
            $(this).attr("data-lock","1");
        }
        
        e.preventDefault();
    });
};

var initOwlCarousel = function() {
    $(".owlCarousel").owlCarousel({
        itemsCustom : [
            [0, 1],
            [820, 2],
            [1200, 3]
        ],
        navigation: true,
        mouseDrag: false,
        navigationText: ["<svg width='45' height='45' xmlns='http://www.w3.org/2000/svg'><g><title>Prev</title><g transform='rotate(90 22.5,22.500000000000004)'><path fill-rule='evenodd' clip-rule='evenodd' d='m41.27401,13.74448c-0.342,0.448 -0.669,1.004 -1.113,1.44101c-5.318,5.247 -10.673,10.457 -15.971,15.724c-1.529,1.519 -2.764,1.322 -4.215,-0.127c-5.052,-5.043 -10.173,-10.018 -15.256,-15.029c-0.34802,-0.34399 -0.67102,-0.74899 -0.87601,-1.18698c-0.117,-0.24902 -0.08301,-0.785 0.08398,-0.90402c0.35001,-0.245 0.99402,-0.54398 1.23602,-0.38699c0.82401,0.534 1.55798,1.22501 2.271,1.91202c4.06201,3.91898 8.11901,7.84297 12.15301,11.78997c2.618,2.562 2.378,2.705 5.002,0.126c4.262,-4.189 8.563,-8.33799 12.864,-12.48799c0.71,-0.686 1.476,-1.32098 2.271,-1.90399c0.189,-0.14001 0.662,-0.07101 0.896,0.07501c0.25,0.15598 0.37,0.51797 0.654,0.95798l0,0l0,-0.00002z' fill='#333'></path></g></g></svg>","<svg width='45' height='45' xmlns='http://www.w3.org/2000/svg' xmlns:svg='http://www.w3.org/2000/svg'><g><title>Next</title><g transform='rotate(-90 22.500005722045902,22.499996185302734)'><path fill-rule='evenodd' clip-rule='evenodd' d='m41.27401,13.74448c-0.342,0.448 -0.669,1.004 -1.113,1.44101c-5.318,5.247 -10.673,10.457 -15.971,15.724c-1.529,1.519 -2.764,1.322 -4.215,-0.127c-5.052,-5.043 -10.173,-10.018 -15.256,-15.029c-0.34802,-0.34399 -0.67102,-0.74899 -0.87601,-1.18698c-0.117,-0.24902 -0.08301,-0.785 0.08398,-0.90402c0.35001,-0.245 0.99402,-0.54398 1.23602,-0.38699c0.82401,0.534 1.55798,1.22501 2.271,1.91202c4.06201,3.91898 8.11901,7.84297 12.15301,11.78997c2.618,2.562 2.378,2.705 5.002,0.126c4.262,-4.189 8.563,-8.33799 12.864,-12.48799c0.71,-0.686 1.476,-1.32098 2.271,-1.90399c0.189,-0.14001 0.662,-0.07101 0.896,0.07501c0.25,0.15598 0.37,0.51797 0.654,0.95798l0,0l0,-0.00002z' fill='#333'/></g></g></svg>"]
    });
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) 
    {
        $( ".owl-buttons" ).hide();
    }
};