$( ".box .button" ).each(function(){
    $pointer = $(this).find("input[type='checkbox']");
    if($pointer.prop("checked"))
    {
        $pointer.attr( "data-check" , "true" );
    }
});

$( ".selectLangBox" ).each(function() {
    $(this).find( "li" ).each(function() {
        $(this).removeClass("inactive");
    });
    $parent = $(this).parent();
    $parent.find( ".selectLangButton span" ).text( $(this).find( "li[data-id='"+ $parent.find( ".selectLangButton input" ).val() +"']" ).addClass("inactive").text());
});

$( ".selectLangButton" ).each(function() {
    $head = $(this).parents().eq("1");
    $head.find( "> .button" ).each(function() {
        $(this).removeClass("inactive");
    });
    $head.find( "> .button span:contains(" + $(this).find( "span" ).text() + ")" ).parent().find( "input" ).prop( "checked" , false).attr( "data-check" , "false" ).parent().addClass("inactive");
});

$( ".selectCategoryBox" ).each(function() {
    $(this).find( "li" ).each(function() {
        $(this).removeClass("inactive");
    });
    $parent = $(this).parent();
    $parent.find( ".selectCategoryButton span" ).text( $(this).find( "li[data-id='"+ $parent.find( ".selectCategoryButton input" ).val() +"']" ).addClass("inactive").text());
});

