<?php

// Uncomment this line if you must temporarily take down your site for maintenance.
// require __DIR__ . '/.maintenance.php';

$container = require __DIR__ . '/../app/bootstrap.php';

$container->getByType('Nette\Application\Application')->run();

//use Nette\Security\Passwords;
//print_r(Passwords::hash("Sem napis heslo, ze ktereho chces udelat hash."));
//exit();

