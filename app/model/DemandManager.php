<?php

namespace App\Model;

use Nette;

class DemandManager extends Nette\Object
{
	const
                TABLE_DEMAND = "demand",
                TABLE_LANG = "lang",
                TABLE_CATEGORY = "category",
                TABLE_LANG_DEMAND_MAP = "lang_demand_map",

                COLUMN_ID = "id",
                COLUMN_ID_LANG = "id_lang",
                COLUMN_ID_SUBMITTER = "id_submitter",
                COLUMN_ID_DEMAND = "id_demand",
                COLUMN_ID_CATEGORY = "id_category",
                COLUMN_NAME = "name",
                COLUMN_TEXT = "text",
                COLUMN_FILE = "file",
                COLUMN_NOTE = "note",
                COLUMN_URGENT = "urgent",
                COLUMN_NOTIFICATION = "notification",
                COLUMN_DATE_OF_REQUEST = "date_of_request",
                COLUMN_DATE_OF_MODIFICATION = "date_of_modification";
                

	/** @var Nette\Database\Context */
	private $database;
	
        /** @var App\Model\FileManager */
	private $fileManager;
        

	public function __construct
        (
            Nette\Database\Context $database,
            FileManager $fileManager
        )
	{
            $this->database = $database;
            $this->fileManager = $fileManager;
	}
        
        public function addDemands($values, $id_user)
        {
            foreach($values['replicator'] as $component)
            {
                $fileName = $this->fileManager->saveFile($component['file']);
                $row = $this->database->table(self::TABLE_DEMAND)->insert(array(
                    self::COLUMN_ID_SUBMITTER => $id_user,
                    self::COLUMN_ID_LANG => $component['langFrom'],
                    self::COLUMN_ID_CATEGORY => $component['category'],
                    self::COLUMN_TEXT => $component['text'],
                    self::COLUMN_FILE => $fileName,
                    self::COLUMN_NOTE => $component['note'],
                    self::COLUMN_URGENT => $component['urgent'],
                    self::COLUMN_NOTIFICATION => $component['notification'],
                    self::COLUMN_DATE_OF_REQUEST => date("Y-m-d H:i:s")
                ));
                $this->mapLangDemand($component['langTo'], $row->id);
            }
        }
        
        private function mapLangDemand($langs, $id_demand)
        {
            foreach($langs as $lang)
            {
                $this->database->table(self::TABLE_LANG_DEMAND_MAP)->insert(array(
                    self::COLUMN_ID_LANG => $lang,
                    self::COLUMN_ID_DEMAND => $id_demand
                ));
            }
        }
        
        public function getSentDemands()
        {
            return array(1,2,3);
        }
        
        public function getCategories()
        {
            return $this->database->table(self::TABLE_CATEGORY)->fetchPairs("id","name");
        }
        
        public function getLangs()
        {
            return $this->database->table(self::TABLE_LANG)->fetchPairs("id","name");
        }
}