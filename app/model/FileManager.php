<?php

namespace App\Model;

use Nette;

class FileManager extends Nette\Object
{
        public function saveFile($fileUpload)
        {
            $filename = $fileUpload->getName();
            if(empty($filename))
            {
                $fileName = '';
            }
            else
            {
                $fileName = substr(md5(uniqid(mt_rand(), true)), 0, 12);
                $fileName .=  '.' . pathinfo($fileUpload->name, PATHINFO_EXTENSION);
                $filePath = dirname(__DIR__) . '../../www/uploads/' . $fileName;
                $fileUpload->move($filePath);
            }
            return $fileName;
        }
}