<?php

namespace App\Model;

use Nette;

class TranslationManager extends Nette\Object
{
	const
                TABLE_DEMAND = "demand",
                TABLE_TRANSLATION = "translation",
                TABLE_LANG = "lang",
                TABLE_USER = "user",
                TABLE_LANG_DEMAND_MAP = "lang_demand_map",
                TABLE_LANG_USER_MAP = "lang_user_map",

                COLUMN_ID = "id",
                COLUMN_NAME = "name",
                COLUMN_ID_DEMAND = "id_demand",
                COLUMN_ID_USER = "id_user",
                COLUMN_ID_LANG = "id_lang";
        
	/** @var Nette\Database\Context */
	private $database;
        
        /** @var App\Model\FileManager */
	private $fileManager;

	public function __construct
        (
                Nette\Database\Context $database,
                FileManager $fileManager
        )
	{
            $this->database = $database;
            $this->fileManager = $fileManager;
	}
        
        private function getLangsOfUser($id_user)
        {
            $langs =  $this->database->table(self::TABLE_LANG_USER_MAP)->where(self::COLUMN_ID_USER,$id_user)->fetchPairs(self::COLUMN_ID_LANG,self::COLUMN_ID_USER);
            return array_keys($langs);
        }
        
        public function getUnsolvedTranslations($id_user)
        {
            $langs = join("','",$this->getLangsOfUser($id_user));
            $sql = "SELECT demand.id, id_submitter, demand.id_lang, id_category, text, file, note, urgent, notification, date_of_request, date_of_modification, lang_demand_map.solved, lang_demand_map.id AS id_map, lang_demand_map.id_lang AS id_lang_map FROM demand JOIN lang_demand_map ON demand.id=id_demand WHERE lang_demand_map.solved=0 AND demand.id_lang IN ('$langs') AND lang_demand_map.id_lang IN ('$langs') ORDER BY demand.urgent DESC, demand.date_of_request ASC";
            $translations  = $this->replaceIdForName($this->database->query($sql)->fetchAll(), "id_lang_map");
            return $this->replaceKeysForId($translations);
        }
        
        public function getSolvedTranslations($id_user)
        {
            $sql = "SELECT translation.id, translation.id_lang, translation.id_translator, translation.id_demand, translation.text, translation.file, translation.date_of_request, translation.date_of_modification, demand.id AS id_demand_demand, demand.id_submitter, demand.text AS text_demand, demand.file AS file_demand, demand.note, demand.urgent FROM translation JOIN demand ON translation.id_demand=demand.id WHERE id_translator=$id_user ORDER BY translation.date_of_request DESC";
            $translations = $this->replaceIdForName($this->database->query($sql)->fetchAll(), "id_lang");
            return $this->replaceKeysForId($translations);
        }
        
        private function replaceIdForName($translations, $column_lang)
        {
            $id_langs = array();
            $id_submitters = array();
            
            foreach ($translations as $key => $translation)
            {
                $id_langs[$key] = $translation[$column_lang];
                $id_submitters[$key] = $translation['id_submitter'];
            }
            $langs = $this->database->table(self::TABLE_LANG)->where(self::COLUMN_ID, $id_langs)->fetchPairs(self::COLUMN_ID, self::COLUMN_NAME);
            $submitters = $this->database->table(self::TABLE_USER)->where(self::COLUMN_ID, $id_submitters)->fetchPairs(self::COLUMN_ID, self::COLUMN_NAME);
            foreach ($translations as $key => $translation)
            {
                $translation['name_lang'] = $langs[$translation[$column_lang]];
                $translation['name_submitter'] = $submitters[$translation['id_submitter']];
            }
            return $translations;
        }
        
        private function replaceKeysForId($translations)
        {
            foreach($translations as $key => $translation)
            {
                $translations[$translation['id']] = $translation;
                unset($translations[$key]);
            }
            return $translations;
        }
        
        public function addTranslation($values, $id_user)
        {
            $fileHash = $this->fileManager->saveFile($values['file']);
            $this->database->table(self::TABLE_TRANSLATION)->insert(array(
                'id_lang' => $values['id_lang'],
                'id_translator' => $id_user,
                'id_demand' => $values['id_demand'],
                'text' => $values['text'],
                'file' => $fileHash,
                'date_of_request' => date("Y-m-d H:i:s")
            ));
            $this->database->table(self::TABLE_LANG_DEMAND_MAP)->where(self::COLUMN_ID_DEMAND, $values['id_demand'])->where(self::COLUMN_ID_LANG, $values['id_lang'])->update(array("solved" => 1));
        }
        
        public function editTranslation($values)
        {
            $fileHash = $this->fileManager->saveFile($values['file']);
            $this->database->table(self::TABLE_TRANSLATION)->where(self::COLUMN_ID,$values["id_translation"])->update(array(
                'text' => $values['text'],
                'file' => $fileHash,
                'date_of_modification' => date("Y-m-d H:i:s")
            ));
        }
        
}