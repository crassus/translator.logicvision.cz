<?php

namespace App\Components\NewDemand;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class NewDemandControl extends Control
{
        private $demandManager;
        private $newDemandLockJS;
        private $categories;
        private $langs;
    
        public function __construct(\App\Model\DemandManager $demandManager)
        {
            $this->demandManager = $demandManager;
            $this->categories = $this->demandManager->getCategories();
            $this->langs = $this->demandManager->getLangs();
        }
    
        public function render()
        {
            $template = $this->getTemplate();
            $template->categories = $this->categories;
            $template->firstCategoryValue = $this->getFirstValue($this->categories);
            $template->firstLangValue = $this->getFirstValue($this->langs);
            $template->newDemandLockJS = $this->newDemandLockJS;
            
            if($this->newDemandLockJS === NULL)
            {
                $this->newDemandLockJS = FALSE;
            }
            
            $template->setFile(__DIR__ . '/NewDemandControl.latte');
            $template->render();
        }
        
        public function createComponentNewDemandForm($name)
        {
            $form = new Form($this, $name);
          
            $form->addSubmit('submit');
            $form->addSubmit('add')
                    ->setValidationScope(FALSE);
            $form->addHidden('items');
            
            $replicator = $form->addContainer('replicator');
            $values = $form->getValues();
            $items = explode(',', $values->items);

            $items = array_filter($items);
            $items = array_map(function($item) { return (int) $item; }, $items);

            $items[0] = 1;

            if ($form['add']->isSubmittedBy()) 
            {
                $items[] = ($items[sizeof($items)-1]+1);
            }
            
            foreach ($items as $i) 
            {
                $container = $replicator->addContainer($i);
                $this->addFieldsToContainer($container);
            }

            foreach ($items as $index => $i) 
            {
                $container = $replicator[$i];

                if ($container['remove']->isSubmittedBy()) 
                {
                    unset($replicator[$i]);
                    unset($items[$index]);

                    break;
                }
            }
            
            if ($form['add']->isSubmittedBy()) 
            {
                $this->setValuesToLastControl($form, $items, "langFrom");
                $this->setValuesToLastControl($form, $items, "langTo");
                $this->setValuesToLastControl($form, $items, "category");
                $this->setValuesToLastControl($form, $items, "note");
                $this->setValuesToLastControl($form, $items, "urgent");
                $this->setValuesToLastControl($form, $items, "notification");
            }
            
            $form['items']->setValue(implode(',', $items));
            
            $form->onSuccess[] = array($this, 'newDemandFormSucceeded');
            
            $this->newDemandLockJS = TRUE;
            
            $this->redrawControl('newDemandForm');
        }
        
        public function newDemandFormSucceeded($form, $values)
        {
            if ($form['submit']->isSubmittedBy()) 
            {
                $this->demandManager->addDemands($values, $this->getPresenter()->getUser()->getIdentity()->id);
                $this->getPresenter()->flashMessage("Úspěšně odesláno");
                $this->getPresenter()->redirect('Submitter:default');
            }
        }
        
        private function addFieldsToContainer($container)
        {
            $container->addHidden('langFrom')
                    ->setDefaultValue($this->getFirstKey($this->langs))
                    ->setRequired("Zadejte prosím jazyk poptávky.");
            $container->addCheckboxList('langTo', 'langTo', $this->langs)
                    ->setDefaultValue($this->getSecondKey($this->categories))
                    ->setRequired("Zadejte prosím všechny jazyky, do kterých chcete nechat přeložit poptávku.");
            $container->addHidden('category')
                    ->setDefaultValue($this->getFirstKey($this->categories))
                    ->setRequired("Zadejte prosím kategorii.");
            $container->addUpload('file');
            $container->addTextArea('text')
                    ->addConditionOn($container['file'], Form::BLANK)
                    ->addRule(Form::REQUIRED,'Zadejte prosím text nebo vložte soubor.');
            $container->addTextArea('note');
            $container->addSubmit('remove')
                    ->setValidationScope(FALSE);
            $container->addCheckbox('urgent');
            $container->addCheckbox('notification');
        }
        
        private function setValuesToLastControl($form, $items, $control)
        {
            $last = $form['replicator'][$items[count($items)-2]][$control]->getValue();
            $form['replicator'][end($items)][$control]->setValue($last);
        }
        
        private function getFirstKey($assoc)
        {
            $keys = array_keys($assoc);
            return $keys[0];
        }
        
        private function getSecondKey($assoc)
        {
            $keys = array_keys($assoc);
            return $keys[1];
        }
        
        private function getFirstValue($assoc)
        {
            $values = array_values($assoc);
            return $values[0];
        }
}
