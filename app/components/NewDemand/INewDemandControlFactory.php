<?php

namespace App\Components\NewDemand;

interface INewDemandControlFactory
{
        /**
         * @return NewDemandControl
         */
        public function create();
}
