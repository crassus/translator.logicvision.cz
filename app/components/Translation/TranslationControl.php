<?php

namespace App\Components\Translation;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class TranslationControl extends Control
{
        private $translationManager;
        private $data;
    
        public function __construct(\App\Model\TranslationManager $translationManager, $data)
        {
            $this->translationManager = $translationManager;
            $this->data = $data;
        }
    
        public function render()
        {
            $template = $this->getTemplate();
            $template->data = $this->data;
            $template->setFile(__DIR__ . '/TranslationControl.latte');
            $template->render();
        }
        
        public function createComponentUnsolvedTranslationForm($name)
        {
            $form = new Form($this, $name);
            $form->addTextArea('text');
            $form->addUpload('file')
                    ->addConditionOn($form['text'], Form::BLANK)
                    ->addRule(Form::REQUIRED,'Zadejte prosím překlad.');
            $form->addHidden('id_lang');
            $form->addHidden('id_demand');
            $form->addSubmit('send');
            $form->onSuccess[] = array($this, 'unsolvedTranslationFormSucceeded');
        }
        
        public function unsolvedTranslationFormSucceeded($form, $values)
        {
            $id_user = $this->getPresenter()->getUser()->getIdentity()->id;
            $this->translationManager->addTranslation($values, $id_user);
            $unsolvedTranslations = $this->translationManager->getUnsolvedTranslations($id_user);
            $solvedTranslations = $this->translationManager->getSolvedTranslations($id_user);
            $this->getPresenter()->setUnsolvedTranslations($unsolvedTranslations);
            $this->getPresenter()->setSolvedTranslations($solvedTranslations);
            $this->getPresenter()->flashMessage("Úspěšně odesláno");
            $this->redrawControl();
            $this->getPresenter()->redrawControl('translations');
            $this->getPresenter()->redrawControl();
        }
        
        public function createComponentSolvedTranslationForm($name)
        {
            $form = new Form($this, $name);
            $form->addTextArea('text')
                    ->setDefaultValue($this->data['text']);
            $form->addUpload('file')
                    ->addConditionOn($form['text'], Form::BLANK)
                    ->addRule(Form::REQUIRED,'Zadejte prosím překlad.');
            $form->addHidden('id_translation');
            $form->addSubmit('save');
            $form->onSuccess[] = array($this, 'solvedTranslationFormSucceeded');
        }
        
        public function solvedTranslationFormSucceeded($form, $values)
        {
            $this->translationManager->editTranslation($values);
            $this->getPresenter()->flashMessage("Úspěšně upraveno");
            $this->redrawControl();
            $this->getPresenter()->redrawControl('translations');
            $this->getPresenter()->redrawControl();
        }
}
