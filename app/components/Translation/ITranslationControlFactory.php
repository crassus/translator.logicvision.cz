<?php

namespace App\Components\Translation;

interface ITranslationControlFactory
{
        /**
         * @return TranslationControl
         */
        public function create($data);
}
