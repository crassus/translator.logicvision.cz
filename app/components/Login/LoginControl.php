<?php

namespace App\Components\Login;

use Nette;
use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class LoginControl extends Control
{
        public function render()
        {
            $template = $this->getTemplate();
            $template->setFile(__DIR__ . '/LoginControl.latte');
            $template->render();
        }
        
        protected function createComponentLoginForm()
        {
            $form = new Form;
            $form->addText("email")
                ->addRule(Form::EMAIL, "Zadejte prosím platný e-mail.")
                ->setRequired("Zadejte prosím email");
            $form->addPassword("password")
                ->addRule(Form::MIN_LENGTH, "Heslo musí mít alespoň %d znaků.", 6)
                ->setRequired("Zadejte prosím heslo.");
            $form->addSubmit("login");
            $form->onSuccess[] = array($this, "loginFormSucceeded");
            return $form;
        }

        public function loginFormSucceeded(Form $form, $values)
        {
            try 
            {
                $this->getPresenter()->getUser()->login($values->email, $values->password);

                if($this->getPresenter()->getUser()->getIdentity()->role == "translator")
                {
                    $this->getPresenter()->redirect('Translator:default');
                }

                if(in_array($this->getPresenter()->getUser()->getIdentity()->role, array("submitter", "admin")))
                {
                    $this->getPresenter()->redirect('Submitter:default');
                }
            } 
            catch (Nette\Security\AuthenticationException $e) 
            {
                $form->addError($e->getMessage());
                $this->redrawControl('loginForm');
            }
        }
}
