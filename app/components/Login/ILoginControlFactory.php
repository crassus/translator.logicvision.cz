<?php

namespace App\Components\Login;

interface ILoginControlFactory
{
        /**
         * @return LoginControl
         */
        public function create();
}
