<?php

namespace App\Components\SentDemand;

interface ISentDemandControlFactory
{
        /**
         * @return SentDemandControl
         */
        public function create($data);
}
