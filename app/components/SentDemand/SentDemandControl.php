<?php

namespace App\Components\SentDemand;

use Nette\Application\UI\Control;
use Nette\Application\UI\Form;

class SentDemandControl extends Control
{
        private $demandManager;
        private $data;
    
        public function __construct(\App\Model\DemandManager $demandManager, $data)
        {
            $this->demandManager = $demandManager;
            $this->data = $data;
        }
    
        public function render()
        {
            $template = $this->getTemplate();
            $template->data = $this->data;
            $template->setFile(__DIR__ . '/SentDemandControl.latte');
            $template->render();
        }
        
        public function createComponentSentDemandForm($name)
        {
            $form = new Form($this, $name);
            $form->addTextArea('text');
            $form->addUpload('file')
                    ->addConditionOn($form['text'], ~Form::FILLED)
                    ->addRule(Form::REQUIRED,'Zadejte prosím překlad.');
            $form->addHidden('id_lang');
            $form->addHidden('id_demand');
            $form->addSubmit('send');
            $form->onSuccess[] = array($this, 'sentDemandFormSucceeded');
        }
        
        public function sentDemandFormSucceeded($form, $values)
        {
            
        }
        
        public function handleSignal()
        {
            
        }
}