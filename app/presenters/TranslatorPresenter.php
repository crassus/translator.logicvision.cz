<?php

namespace App\Presenters;

use Nette\Application\UI\Multiplier;

class TranslatorPresenter extends BasePresenter
{
        /** @var App\Components\Translation\ITranslationControlFactory */
        private $translationControlFactory;
        
        /** @var App\Model\TranslationManager */
        private $translationManager;
        
        /** @var Array */
        private $unsolvedTranslations;
        
        /** @var Array */
        private $solvedTranslations;
        
        public function __construct
        (
            \App\Components\Translation\ITranslationControlFactory $translationControlFactory,
            \App\Model\TranslationManager $translationManager
        )
	{
            parent::__construct();
            $this->translationControlFactory = $translationControlFactory;
            $this->translationManager = $translationManager;
        }
        
        public function actionDefault()
        {
            if(!$this->getUser()->isLoggedIn())
            {
                $this->redirect('Homepage:default');
            }
            
            if($this->getUser()->getIdentity()->role !== "translator")
            {
                $this->redirect('Submitter:default');
            }
            $this->unsolvedTranslations = $this->translationManager->getUnsolvedTranslations($this->getUser()->getIdentity()->id);
            $this->solvedTranslations = $this->translationManager->getSolvedTranslations($this->getUser()->getIdentity()->id);
        }
        
        public function renderDefault()
        {
            $this->template->unsolvedTranslations = $this->unsolvedTranslations;
            $this->template->solvedTranslations = $this->solvedTranslations;
        }
        
        public function createComponentUnsolvedTranslationControl()
        {
            return new Multiplier(function ($key) {
                return $this->translationControlFactory->create($this->unsolvedTranslations[$key]);
            });
        }
        
        public function createComponentSolvedTranslationControl()
        {
            return new Multiplier(function ($key) {
                return $this->translationControlFactory->create($this->solvedTranslations[$key]);
            });
        }
        
        public function setUnsolvedTranslations($translations)
        {
            $this->unsolvedTranslations = $translations;
        }
        
        public function setSolvedTranslations($translations)
        {
            $this->solvedTranslations = $translations;
        }
}