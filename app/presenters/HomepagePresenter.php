<?php

namespace App\Presenters;

class HomepagePresenter extends BasePresenter
{
        private $loginControlFactory;
    
        public function __construct(\App\Components\Login\ILoginControlFactory $loginControlFactory)
	{
            parent::__construct();
            $this->loginControlFactory = $loginControlFactory ;
        }
        
        public function actionDefault()
        {
            
            if($this->getUser()->isLoggedIn())
            {
                if($this->getUser()->getIdentity()->role == "translator")
                {
                    $this->redirect('Translator:default');
                }

                if(in_array($this->getUser()->getIdentity()->role, array("submitter", "admin")))
                {
                    $this->redirect('Submitter:default');
                }
            }
        }
        
        protected function createComponentLoginControl()
        {
            return $this->loginControlFactory->create();
        }
}
