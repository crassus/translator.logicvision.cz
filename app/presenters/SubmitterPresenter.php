<?php

namespace App\Presenters;

use Nette\Application\UI\Multiplier;

class SubmitterPresenter extends BasePresenter
{      
        /** @var App\Components\NewDemand\INewDemandControlFactory */
        private $newDemandControlFactory;
        
        /** @var App\Components\SentDemand\ISentDemandControlFactory */
        private $sentDemandControlFactory;
        
        /** @var App\Model\DemandManager */
        private $demandManager;
        
        /** @var Array */
        private $sentDemands;

        public function __construct
        (
            \App\Components\NewDemand\INewDemandControlFactory $newDemandControlFactory,
            \App\Components\SentDemand\ISentDemandControlFactory $sentDemandControlFactory,
            \App\Model\DemandManager $demandManager
        )
	{
            parent::__construct();
            $this->newDemandControlFactory = $newDemandControlFactory;
            $this->sentDemandControlFactory = $sentDemandControlFactory;
            $this->demandManager = $demandManager;
        }
        
        public function actionDefault()
        {            
            if(!$this->getUser()->isLoggedIn())
            {
                $this->redirect('Homepage:default');
            }
            
            if(!in_array($this->getUser()->getIdentity()->role, array("submitter", "admin")))
            {
                $this->redirect('Translator:default');
            }
            $this->sentDemands = $this->demandManager->getSentDemands();
        }
        
        public function renderDefault()
        {
            $this->template->sentDemands = $this->sentDemands;
        }
        
        public function createComponentNewDemandControl()
        {
            return $this->newDemandControlFactory->create();
        }
        
        public function createComponentSentDemandControl()
        {
            return new Multiplier(function ($key) {
                return $this->sentDemandControlFactory->create($this->sentDemands[$key]);
            });
        }
}