<?php

namespace App\Presenters;

use Nette;
use Nette\Utils\Strings;

abstract class BasePresenter extends Nette\Application\UI\Presenter
{
        public function actionLogout()
	{
            $this->getUser()->logOut();
            $this->redirect('Homepage:default');
	}
        
        public function splitName($name)
        {
            $res = Strings::split($name, "~ \s*~");
            return $res[0];
        }
}
